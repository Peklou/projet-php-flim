create table film(
  idf number(5),
  titre varchar(40),
  realisateur varchar(40),
  annee number(4),
  genre varchar(20),
  description varchar(400),
  urlimage varchar(100)
);
create table acteur(
  ida number(5),
  nom varchar(20),
  prenom varchar(20)
);
create table apparrait(
  ida number(5),
  idf number(5)
);
